var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tablefix = new Schema({
    day:{
        type: Number
    },
    lottery: {
        type: Number
    },
    timeOpen:{
        type: String
    },
    timeClose:{
        type: String
    },
    open:{
        type:Number
    },
    time_create:{
        type:Number
    }
});

module.exports = mongoose.model('tablefix', tablefix);
