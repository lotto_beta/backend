var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var result = new Schema({
  lottery_id: {
    type: Number
  },
  round:{
    type: Number
  },
  title_lottery:{
    type: String
  },
  tableid:{
    type: String
  },
  table_timeopen:{
    type: Number
  },
  lottery:{
    type: String
  },
  threeb:{
    type: String
  },
  threel:{
    type: String
  },
  twol:{
    type: String
  },
  time_create:{
    type: Number
  }
});

module.exports = mongoose.model('result', result);
