var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var listSchema = new Schema({
    lottery:{
        type: Number
    },
    userid:{
        type: String
    },
    username:{
        type: String
    },
    threeBFull: {
        type: Number
    },
    threeTFull: {
        type: Number
    },
    threeLFull: {
        type: Number
    },
    twoBFull: {
        type: Number
    },
    twoLFull: {
        type: Number
    },
    runBFull: {
        type: Number
    },
    runLFull: {
        type: Number
    },
    threeBMaximum: {
        type: Number
    },
    threeTMaximum: {
        type: Number
    },
    threeLMaximum: {
        type: Number
    },
    twoBMaximum: {
        type: Number
    },
    twoLMaximum: {
        type: Number
    },
    runBMaximum: {
        type: Number
    },
    runLMaximum: {
        type: Number
    },
    threeBFullPay:{
        type: Number
    },
    threeTFullPay:{
        type: Number
    },
    threeLFullPay:{
        type: Number
    },
    twoBFullPay:{
        type: Number
    },
    twoLFullPay:{
        type: Number
    },
    runBFullPay:{
        type: Number
    },
    runLFullPay:{
        type: Number
    },
});

module.exports = mongoose.model('fullmember', listSchema);
