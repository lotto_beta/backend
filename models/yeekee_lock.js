var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var yeekee_lock = new Schema({
  threeb:{
    type: String
  },
  twol:{
    type: String
  },
  round:{
    type: Number
  },
  tableid:{
    type: String
  }
});

module.exports = mongoose.model('yeekee_lock', yeekee_lock);
