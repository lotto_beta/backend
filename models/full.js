var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var full = new Schema({
  number: {
    type: Number
  },
  type:{
    type: String
  },
  lottery:{
    type: Number
  },
  pay:{
    type: Number
  }
});

module.exports = mongoose.model('full', full);
