var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var yeekee = new Schema({
  id:{
    type: String
  },
  tableid:{
    type: String
  },
  round:{
    type: Number
  },
  dateOpen:{
    type: Number
  },
  open:{
    type: Number
  }
});

module.exports = mongoose.model('yeekee', yeekee);
