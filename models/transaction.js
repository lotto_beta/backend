var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var transaction = new Schema({
  bank:{
    type: String
  },
  amount:{
    type: Number
  },
  balance:{
    type: Number
  },
  nowtrans:{
    type: String
  },
  timedeposit:{
    type: String
  },
  note:{
    type: String
  },
  bank:{
    type: String
  },
  bankno:{
    type: String
  },
  bankname:{
    type: String
  },
  userid: {
    type: String
  },
  username: {
    type: String
  },
  type: {
    type: Number
  },
  status:{
    type: Number
  },
  notification:{
    type: Number
  }
});

module.exports = mongoose.model('transaction', transaction);
