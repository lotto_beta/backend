var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var aff_click = new Schema({
  userid:{
    type: String
  },
  time_click:{
    type: Number
  }
});

module.exports = mongoose.model('aff_click', aff_click);
