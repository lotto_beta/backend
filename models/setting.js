var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var settingSchema = new Schema({
    withdraw_min: {
        type: Number
    },
    deposit_min: {
        type:Number
    },
    withdraw_income_min:{
        type:Number
    },
    contact:{
        type:String
    },
    condition:{
        type:String
    },
    message_maqueen:{
        type:String
    },
    logo:{
        type:String
    },
    logo_header:{
        type:String
    },
    aff_percent:{
        type:Number
    },
    vip_i:{
        type:Number
    },
    vip_ii:{
        type:Number
    },
    vip_iii:{
        type:Number
    },
    vip_iv:{
        type:Number
    }
});

module.exports = mongoose.model('setting', settingSchema);
