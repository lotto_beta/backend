var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var information = new Schema({
  title:{
    type: String
  },
  body:{
    type: String
  },
  time_created:{
    type: Number
  },
  createby:{
    type: String
  }
});

module.exports = mongoose.model('information', information);
