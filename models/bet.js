var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bet = new Schema({
  num: {
    type: Array
  },
  lottery:{
    type: Number
  },
  round:{
    type: Number
  },
  title:{
    type: String
  },
  get_amount:{
    type: Number
  },
  bet_amount:{
    type: Number
  },
  userid:{
    type: String
  },
  username:{
    type: String
  },
  date:{
    type: Number
  },
  timenotice:{
    type: Number
  },
  timebet:{
    type: Number
  },
  status:{
    type:Number
  },
  tableid:{
    type:String
  },
  aff_userid:{
    type:String
  },
  aff_amount:{
    type: Number
  }
  
});

module.exports = mongoose.model('bet', bet);
