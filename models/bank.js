var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bank = new Schema({
  id:{
    type: String
  },
  bank:{
    type: String
  },
  bankno:{
    type: String
  },
  bankname:{
    type: String
  }
});

module.exports = mongoose.model('bank', bank);
