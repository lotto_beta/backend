var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var number_sets = new Schema({
    name_sets:{
        type: String
    },
    numbers:{
        type: Array
    },
    userid:{
        type: String
    },
    time_create:{
        type: Number
    }
});

module.exports = mongoose.model('number_sets', number_sets);
