var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var group = new Schema({
  id:{
    type: String
  },
  title:{
    type: String
  }
});

module.exports = mongoose.model('group', group);
