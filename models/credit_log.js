var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var creditLog = new Schema({
  detail: {
    type: Number
  },
  by:{
    type: String
  },
  userid:{
    type: String
  },
  username:{
    type: String
  },
  credit:{
    type: Number
  },
  balance:{
    type: String
  },
  time_create:{
    type: Number
  },
  betid:{
    type: String
  },
  note:{
    type: String
  },
  mode:{
    type: Number
  }
  
});

module.exports = mongoose.model('credit_log', creditLog);
