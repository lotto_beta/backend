var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var listSchema = new Schema({
    id: {
        type: Number
    },
    title: {
        type: String
    },
    body:{
        type: String
    },
    group: {
        type: Number
    },
    type: {
        type: Array
    },
    run: {
        type:Array
    },
    two: {
        type:Array
    },
    three: {
        type:Array
    },
    open:{
        type: String
    }
});

module.exports = mongoose.model('list', listSchema);
