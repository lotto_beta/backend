var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var closed = new Schema({
  number: {
    type: Number
  },
  type:{
    type: String
  },
  lottery:{
    type: Number
  },
  amount:{
    type: Number
  }
});

module.exports = mongoose.model('closed', closed);
