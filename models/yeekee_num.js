var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var yeekee_num = new Schema({
  num:{
    type: String
  },
  userid:{
    type: String
  },
  username:{
    type: String
  },
  time_send:{
    type: Number
  },
  round:{
    type: Number
  },
  tableid:{
    type: String
  }
});

module.exports = mongoose.model('yeekee_num', yeekee_num);
