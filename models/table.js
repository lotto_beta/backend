var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tableSchema = new Schema({
    lottery: {
        type: Number
    },
    title:{
        type: String
    },
    open:{
        type: Number
    },
    group:{
        type: Number
    },
    date_open:{
        type: Number
    },
    date_close:{
        type: Number
    },
    notice:{
        type: Number
    }
});

module.exports = mongoose.model('table', tableSchema);
