var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email:{
        type: String,
    },
    firstname:{
        type:String
    },
    lastname:{
        type:String
    },
    birthday:{
        type:Number
    },
    telephone:{
        type: String,
        require: true
    },
    bank:{
        type: String,
        required: true
    },
    bankno:{
        type: String,
        required: true
    },
    bankname:{
        type: String,
        required: true
    },
    credit:{
        type:Number
    },
    aff_userid:{
        type:String
    },
    token: {
        type:String
    },
    ip: {
        type:String
    },
    status:{
        type:Number
    },
    date_register:{
        type:Number
    },
    income:{
        type:Number
    },
    last_login:{
        type:Number
    }
});

UserSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) return next(err);
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                if (err) return next(err);
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', UserSchema);