var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var log = new Schema({
  user:{
    type:String
  },
  ipaddress:{
    type:String
  },
  browser:{
    type:String
  },
  time_create:{
    type:Number
  },
  mode:{
    type:String
  }
});

module.exports = mongoose.model('log', log);
