var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();

var User = require("../models/user");
var Bet = require("../models/bet");
var ListLottery = require("../models/list");
var Transaction = require("../models/transaction");
var Bank = require("../models/bank");
var Group = require("../models/group");
var Table = require("../models/table");
var Full = require("../models/full");
var FullAll = require("../models/fullall");
var FullMember = require("../models/fullmember");
var Information = require("../models/information");

var YeeKeeNumber = require("../models/yeekee_num");
var YeeKee = require("../models/yeekee");
var YeeKeeLock = require("../models/yeekee_lock");

var AffClick = require('../models/aff_click');
var Settings = require("../models/setting");
var Result = require("../models/result");
var Closed = require('../models/closed');
var UserRole = require('../models/user_role');

var permutations = require('permutation');
var unique = require('array-unique');
var NumberSets = require("../models/number_sets")
var Log = require("../models/log");

var bcrypt = require('bcrypt-nodejs');
var jwtDecode = require('jwt-decode');
var Tablefix = require('../models/tablefix');
var CreditLog = require('../models/credit_log');

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey("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"); //send mail

let thmonth = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];

router.post('/signup', function(req, res) {
  try {
    if (!req.body.username || !req.body.password || !req.body.bank || !req.body.bankno || !req.body.bankname || !req.body.telephone) {
      res.json({success: false, msg: 'กรุณากรอกข้อมูลให้ครบถ้วน'});
    } else {
      User.findOne({
        username:req.body.username
      },function(err,user){
        if(!user){
          if(req.body.password == req.body.repassword){
            var newUser = new User({
              username: req.body.username,
              password: req.body.password,
              email: req.body.email,
              bank: req.body.bank,
              bankno: req.body.bankno,
              bankname: req.body.bankname,
              telephone: req.body.telephone,
              aff_userid: req.body.aff_userid,
              credit:0,
              income:0,
              ip: req.body.ipaddress,
              status:0,
              date_register: new Date().getTime(),
              token:""
            });

            // save the user
            newUser.save(function(err) {
              if (err) {
                return res.json({success: false, msg: err});
              }
              res.json({success: true, msg:'สมัครสมาชิกเรียบร้อยแล้ว'});
            });  
          } else {
            res.json({success: false, msg:'รหัสผ่านทั้งสองช่องไม่ตรงกัน'});
          }
        } else {
          res.json({success: false, msg:'Username ซ้ำกับในระบบ'});
        }
      })
    }    
  } catch (error) {
    res.json({success: false, msg: error});
    console.log(error);
  }

});

router.post('/signin', function(req, res) {
  User.findOne({
    username: req.body.username
  }, function(err, user) {
    try {
      if(!user){
        res.json({success: false , msg: 'โปรดตรวจสอบข้อมูลการล็อกอินให้ถูกต้อง!'})
        return false
      }

      if(!user.status){
          // check if password matches
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            // return the information including token as JSON

            var token = jwt.sign({'id':user._id}, config.secret);
            User.updateOne({ username : req.body.username },{ $set: { token : token , last_login:new Date().getTime() }}, function(err,res){});

            res.json({success: true, token: 'JWT ' + token , msg: 'เข้าสู่ระบบเรียบร้อย'});
          } else {
            res.json({success: false, msg: 'รหัสผ่านผิดพลาด'});
          }
        });
      } else {
        res.json({success: false , msg: 'ไม่สามารถเข้าสู่ระบบได้กรุณาติดต่อผู้ดูแลระบบ' })
      }  
    } catch (error) {
      
    }
  });
});

router.post('/change-password',function(req,res){
  User.findOne({
    _id: req.body.id
  }, function(err, user) {
    if(req.body.new_password != req.body.cf_new_password){
      res.json({success:false,msg:'รหัสผ่านทั้งสองช่องไม่ตรงกัน'})
    } else {
      user.comparePassword(req.body.old_password, function (err, isMatch) {
        if (isMatch && !err) {
          bcrypt.genSalt(10, function (err, salt) {
            if (err) console.log(err)
            bcrypt.hash(req.body.new_password, salt, null, function (err, hash) {
              if (err) console.log(err)
              User.updateOne({ _id : user._id },{ $set: { password : hash }}, function(err){});
              console.log(user.username + " " + user.password + " " + hash)
              res.json({success:true})
            });
          });
        } else {
          res.json({success: false, msg: 'รหัสผ่านผิดพลาด'});
        }
      });      
    }
  })
})

router.post('/forgot',function(req,res){
  User.findOne({
    email: req.body.email
  }, function(err, user) {
    if(user){
      let password = randomString(10)
      newpassandsendmail(user.email,password)
      bcrypt.genSalt(10, function (err, salt) {
        if (err) console.log(err)
        bcrypt.hash(password, salt, null, function (err, hash) {
          if (err) console.log(err)
          User.updateOne({ _id : user._id },{ $set: { password : hash }}, function(err){});
          res.json({success:true,msg:"ระบบได้ทำการส่งรหัสผ่านใหม่ให้คุณเรียบร้อยแล้ว",newpassword: password})
        });
      });
    } else {
      res.json({success:false,msg:'ไม่พบอีเมล์นี้ในระบบ'})
    }
  })
})

function randomString(len) {
  const charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var randomString = '';
  for (var i = 0; i < len; i++) {
      var randomPoz = Math.floor(Math.random() * charSet.length);
      randomString += charSet.substring(randomPoz,randomPoz+1);
  }
  return randomString;
}

function newpassandsendmail(mail,password){
  const msg = {
      to: mail,
      from: 'support@huay289.net',
      subject: 'รหัสผ่านใหม่',
      text:  'รหัสผ่านใหม่ของคุณคือ ' + password ,
    };
    sgMail.send(msg);
  return true
}

router.post('/save-profile',function(req,res){
  
  User.findOne({
    _id:req.body.id
  },function(err,user){
    User.updateOne({_id:req.body.id} , { $set:{ email:req.body.email , telephone:req.body.telephone }},function(err){
      if(err){
        res.json({success:false,msg:err})
      } else {
        res.json({success:true}) 
      }
    })
  })
})

router.post('/checkuser',function(req,res){
  User.findOne({ 
    _id:req.body.id 
  },function(err,user){
    if(user){
      res.json({success: true});
    } else {
      res.json({success: false});
    }
  });
});

router.post('/resetpassword',function(req,res){
  User.findOne({
    _id:req.body.id
  },function(err,user){
    let password = randomString(10)
    newpassandsendmail(user.email,password)
    bcrypt.genSalt(10, function (err, salt) {
      if (err) console.log(err)
      bcrypt.hash(password, salt, null, function (err, hash) {
        if (err) console.log(err)
        User.updateOne({ _id : user._id },{ $set: { password : hash }}, function(err){});
        res.json({success:true,msg:"ระบบได้ส่ง รหัสผ่านไปยังอีเมล์ของสมาชิกแล้ว", password: password})
      });
    });
  })
})

router.post('/save-click',function(req,res){
  User.findOne({
    _id:req.body.id
  },function(err,user){
    aff_click = new AffClick({
      userid:req.body.id,
      time_click:new Date().getTime()
    })

    aff_click.save(function(err){
      res.json({success:true})
    })
  })
})

router.post('/aff-data',function(req,res){

  AffClick.count({
    userid:req.body.id
  },function(err,acount){
    User.count({
      aff_userid:req.body.id
    },function(err,ucount){
        res.json({'aff_click':acount,'user_count':ucount})
    })
  })
})

router.post('/aff-totalget',function(req,res){
  var total = 0
  Bet.find({
    aff_userid:req.body.id
  },function(err,bet){
    bet.forEach(d => {
      if(d.status == 1){
        total += d.aff_amount  
      }
    })
    res.json(total)
  })
})

router.post('/aff-invited',function(req,res){
  var userD = Array()
  var totalBet = 0
  User.find({
    aff_userid:req.body.id
  },function(err,user){  
    user.forEach(u => {
      userD.push({'id':u._id,'username':u.username,'register_date':u.date_register,'totalbet':0})  
    })
    res.json(userD)
  })
})

router.post('/total-bet',function(req,res){
  var total = 0
  Bet.find({
    userid:req.body.id,
    status:1
  },function(err,bet){
    bet.forEach(element => {
      total += element.bet_amount
    });
    res.json({'totalbet':total})
  })
})

router.post('/aff-income',function(req,res){
  Bet.find({
    aff_userid:req.body.id,
    status:1
  },function(err,bet){
    bet.forEach(element => {
      console.log(element.timenotice)
    });
  })
})

router.post('/transactions-income',function(req,res){
  Transaction.find({
    userid:req.body.id,
    type:3
  }).sort({nowtrans:-1}).exec(function(err,trans){
    res.json(trans)
  })
})

router.post('/withdraw-income',function(req,res){
  User.findOne({
    _id:req.body.id
  },function(err,user){
    Settings.findOne({

    },function(err,settings){
      if(req.body.amount > settings.withdraw_income_min){
        if(user.income > req.body.amount){
          var income = user.income - req.body.amount
          User.updateOne({ _id:user._id },{ $set : { income:income }},function(err,res){
            var withdraw = new Transaction({
              amount: req.body.amount,
              nowtrans: new Date().getTime(),
              userid: req.body.id,
              username: user.username,
              balance:income,
              type:3, // ประเภท ฝากเงิน
              notification:0,
              status:0
            });

            withdraw.save(function(err){})
          })

          res.json({success:true})
        } else {
          res.json({success:false,msg:'จำนวนรายได้ไม่พอ'})
        }        
      } else {
        res.json({success:false,msg:'จำนวนที่ต้องการถอนต่ำกว่าจำนวนถอนขั้นต่ำ'})
      }
    })
  })

})

router.post('/create-numbersets',function(req,res){
  var numbersets = new NumberSets({
    name_sets:req.body.name_sets,
    numbers:req.body.numbers,
    userid:req.body.userid,
    time_create:new Date().getTime()
  })

  numbersets.save(function(err){})
  res.json({success:true})
})

router.post('/list-numbersets',function(req,res){
  NumberSets.find({
    userid:req.body.id
  },function(err,number){
    res.json(number)
  })  
})

router.post('/list-numbersetsfromid',function(req,res){
  NumberSets.findOne({
    _id:req.body.id,
    userid:req.body.userid
  },function(err,number){
    res.json(number)
  })  
})

router.post('/delete-numbersets',function(req,res){
  NumberSets.deleteOne({_id:req.body.id} , function(err,res){})
  res.json({success:true})
})

router.get('/list-settings',function(req,res){
  Settings.findOne({

  },function(err,data){
    let settings = {
      aff_percent: data.aff_percent,
      condition: data.condition,
      contact: data.contact,
      deposit_min: data.deposit_min,
      logo: data.logo,
      logo_header: data.logo_header,
      message_maqueen: data.message_maqueen,
      withdraw_income_min: data.withdraw_income_min,
      withdraw_min: data.withdraw_min,
      vip_i: data.vip_i,
      vip_ii: data.vip_ii,
      vip_iii: data.vip_iii,
      vip_iv: data.vip_iv
    }
    res.json(settings)
  })
})

router.post('/save-settings',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'save-setiings'
  })

  log.save(function(err){})

  Settings.findOne({
  },function(err,settings){
    if(settings != null){
      Settings.updateOne({_id:settings._id} , { $set:{ aff_percent:req.body.aff_percent , withdraw_min:req.body.withdraw , deposit_min:req.body.deposit , withdraw_income_min: req.body.withdraw_income , contact: req.body.contact , condition:req.body.condition , message_maqueen:req.body.message_maqueen,logo:req.body.logo,logo_header:req.body.logo_header, vip_i:req.body.vip_i , vip_ii:req.body.vip_ii , vip_iii:req.body.vip_iii , vip_iv:req.body.vip_iv}},function(err){
        if(err){
          res.json({success:false})
        } else {
          res.json({success:true})
        }
      })
    } else {
      st = new Settings({
        aff_percent:req.body.aff_percent , 
        withdraw_min:req.body.withdraw , 
        deposit_min:req.body.deposit , 
        withdraw_income_min: req.body.withdraw_income , 
        contact: req.body.contact , 
        condition:req.body.condition ,
        message_maqueen:req.body.message_maqueen,
        logo:req.body.logo,
        logo_header:req.body.logo_header,
        vip_i:req.body.vip_i , 
        vip_ii:req.body.vip_ii , 
        vip_iii:req.body.vip_iii , 
        vip_iv:req.body.vip_iv
      })

      st.save(function(err){
        if(err) { 
          res.json({success:false}) 
        } else {
          res.json({success:true})  
        }
      })      
    }
  })
})

function betTotal(userid){
  return new Promise(async (resolve) => {
    let betTotal = 0
    Bet.find({
      userid: userid,
      status:1
    }, function (err, bet) {
      for (let x of bet) {
        betTotal += x.bet_amount
      }
      resolve(betTotal)
    })
  })
}

function vipSetting(){
  return new Promise(async (resolve) => {
    Settings.findOne({

    },function(err,settings){
      let vip = {
        vip_i:settings.vip_i,
        vip_ii:settings.vip_ii,
        vip_iii:settings.vip_iii,
        vip_iv:settings.vip_iv
      }
      resolve(vip)

    })
  })
}

router.get('/auth', async(req, res) => {
  var token = getToken(req.headers);
  if (token) {
    var userid = jwtDecode(token)
    User.findOne({
      _id: userid.id
    },async(err,user) => {
      if(err) throw err;
      try {
        if(user.status == 0){
          if(user){
            const betTotalResult = await betTotal(user.id)
            const vipSettings = await vipSetting()
            let vipRank = 0

            let vip_i_Progress = 0
            let vip_ii_Progress = 0
            let vip_iii_Progress = 0
            let vip_iv_Progress = 0

            if (betTotalResult >= 0 && betTotalResult <= vipSettings.vip_i) {
              vipRank = 0
              vip_i_Progress = (betTotalResult * 100) / vipSettings.vip_i
            }
    
            if (betTotalResult >= vipSettings.vip_i && betTotalResult <= vipSettings.vip_ii) {
              vipRank = 1
              vip_i_Progress = 100
              vip_ii_Progress = (betTotalResult * 100) / vipSettings.vip_ii
            }
    
            if (betTotalResult >= vipSettings.vip_ii && betTotalResult <= vipSettings.vip_iii) {
              vipRank = 2
              vip_i_Progress = 100
              vip_ii_Progress = 100
              vip_iii_Progress = (betTotalResult * 100) / vipSettings.vip_iii
            }
            if (betTotalResult >= vipSettings.vip_iii && betTotalResult <= vipSettings.vip_iv) {
              vipRank = 3;
              vip_i_Progress = 100
              vip_ii_Progress = 100
              vip_iii_Progress = 100
              vip_iv_Progress = (betTotalResult) / 100
            }

            let result = {
              id:user.id,
              username:user.username,
              token:user.token,
              aff_userid:user.aff_userid,
              date_register: user.date_register,
              status: user.status,
              income: user.income,
              credit: user.credit,
              telephone: user.telephone,
              bankname: user.bankname,
              bankno: user.bankno,
              bank: user.bank,
              email: user.email,
              bettotal: betTotalResult,
              vip:vipRank,
              vip_i_Progress:vip_i_Progress,
              vip_ii_Progress:vip_ii_Progress,
              vip_iii_Progress:vip_iii_Progress,
              vip_iv_Progress:vip_iv_Progress,
            }

            res.json({ success:true , msg: result})
          } else {
            res.json({ success:false })
          } 
        } else {
          res.json({success: false })
        }  
      } catch (error) {
        res.json({ success: false })
      }
    });
    
  } else {
    res.json({ success: false });
  }
});

router.get('/list-lottery', function(req, res) {
  var lottery = []
  ListLottery.find({
  }).sort({id:1}).exec(function(err, data){ 
    if (err) console.log(err)
    data.forEach(element => {
      lottery.push({_id:element._id,id:element.id,title:element.title,group:element.group})  
    });
    
    res.json(lottery);
  });
});

router.get('/list-lottery/:id', function(req, res) {
  ListLottery.findOne({
    id: req.params.id
  }, function(err, data) {
    if (err) console.log(err)
    res.json(data);
  });
});

router.post('/list-lotterybyid',function(req,res){
  ListLottery.findOne({ 
    id:req.body.id 
  } , function(err, data){
    if (err) console.log(err)
    res.json(data)
  });
});



router.post('/create-lottery',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'create-lottery'
  })

  log.save(function(err){})

  var list = new ListLottery({
    id:req.body.id,
    title:req.body.title,
    group:req.body.group,
    type:req.body.type,
    three:req.body.three,
    two:req.body.two,
    run:req.body.run
  });
  // save the user
  list.save(function(err) {
    if (err) res.json({success: false });
    res.json({success: true });
  });
});

router.post('/edit-lottery',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'edit-lottery'
  })

  log.save(function(err){})
  ListLottery.updateOne({ id : req.body.id },{ $set: { title:req.body.title , body:req.body.body , group:req.body.group , three:req.body.three , two:req.body.two , run:req.body.run , type:req.body.type} }, function(err,res){
    if(err) console.log(err)
  });

  res.json({success: true})
});

router.post('/delete-lottery',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'delete-lottery'
  })

  log.save(function(err){})

  var id = req.body.id;
  ListLottery.deleteOne({ id: id } , function(err, res){
    if(err) console.log(err)
  })

  res.json({ success:true });
});

router.post('/create-group',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'create-group'
  })

  log.save(function(err){})

  var group = new Group({
    id:req.body.id,
    title:req.body.title
  });
  // save the user
  group.save(function(err) {
    if (err) {
      return res.json({success: false, msg: err});
    }
    res.json({success: true, msg: 'สร้างกลุ่มใหม่เรียบร้อยแล้ว'});
  });
});

router.post('/delete-group',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'delete-group'
  })

  log.save(function(err){})

  var id = req.body.id;
  Group.deleteOne({ _id: id } , function(err, res){
    if(err) console.log(err)
  })

  res.json({ success:true });
});

router.get('/list-group',function(req, res){
  Group.find(function(err, data){
    if(err) console.log(err)
    res.json(data);
  });
});

router.post('/bet', function(req, res) {
  var token = req.body.token
  if (token) {
    try{
    var userid = jwtDecode(token)
    User.findOne({
      _id: userid.id
      },function(err,user){
        if(err) console.log(err);
        if(user){
          Table.findOne({
            _id:req.body.tableid
          },function(err,lottery){
            if(err) console.log(err);

            if(user.credit >= req.body.betAmount && user.credit != 0){
              var credit = user.credit - req.body.betAmount;
              User.updateOne({ username : user.username },{ $set: { credit : credit }}, function(err,res){});
              
              var lotteryDate

              if(lottery.lottery == 2){
                lotteryDate = lottery.date_open
              } else {
                lotteryDate = lottery.date_close
              }

              Settings.findOne({

              },function(err,settings){
                if(err) console.log(err)
                let aff_amount = 0
                if(user.aff_userid) aff_amount = (req.body.betAmount * settings.aff_percent) / 100
                var bet = new Bet({
                  num: req.body.num,
                  lottery: lottery.lottery,
                  title: lottery.title,
                  round:req.body.round,
                  get_amount: '0',
                  bet_amount: req.body.betAmount,
                  userid: user._id,
                  username: user.username,
                  date:lotteryDate,
                  timenotice:0,
                  timebet: new Date().getTime(),
                  tableid:req.body.tableid,
                  aff_userid:user.aff_userid,
                  aff_amount:aff_amount,
                  status:0
                });
              
                bet.save(function(err,b) {
                  if (err) res.json({success: false, msg: 'ไม่สามารถทำรายการได้ ' + err});
                  var note = ""

                  if(lottery.lottery == 2) {
                    note = 'แทงพนัน ' + lottery.title + ' รอบที่ ' + req.body.round + ' / ' + new Date(lottery.date_close).getDate() + " " + thmonth[new Date(lottery.date_close).getMonth()] + " " + (new Date(lottery.date_close).getFullYear() + 543)
                  } else {
                    note = 'แทงพนัน ' + lottery.title + ' / ' + new Date(lottery.date_close).getDate() + " " + thmonth[new Date(lottery.date_close).getMonth()] + " " + (new Date(lottery.date_close).getFullYear() + 543)
                  }
                  
                  creditlog = new CreditLog({
                    detail:3,
                    by:'',
                    userid:user._id,
                    username:user.username,
                    credit:req.body.betAmount,
                    balance:(user.credit - req.body.betAmount),
                    time_create:new Date().getTime(),
                    betid:b._id,
                    note:note,
                    mode:2
                  })

                  creditlog.save(function(err){
                    if(err) console.log(err); else res.json({success: true,id:b._id});
                  })

                });
              })
            } else {
              res.json({success: false , msg: 'จำนวนเครดิตไม่เพียงพอ'});
            }
          });        
        } else {
          res.json({success:false , msg: 'กรุณาล็อกอินเข้าสู่ระบบใหม่'})
          // User.updateOne({ username : req.body.username },{ $set: { credit : credit }}, function(err,res){});
        }
      });    
    } catch (error) {
      
    }
  }
});

function diffTime(date1,date2){
  var difference = date1 - date2;

  var minutesDifference = Math.floor(difference/1000/60);
  difference -= minutesDifference*1000*60

  return minutesDifference
}

router.post('/cancel-betall',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'cancel-betall'
  })

  log.save(function(err){})

  Bet.find({
    tableid:req.body.tableid
  },function(err,bet){
    bet.forEach(b => {
      if(b.status != 2){
        Bet.updateOne({ _id:b._id } , { $set:{ status : 2 }},function(err){})
        
        if(b.bet_amount != 0){
          User.findOne({
            _id:b.userid
          },function(err,u){

            //รับเงินเดิมพันคืน
            User.updateOne({_id:b.userid} , { $inc: {credit:b.bet_amount}},function(err){})

            if(b.get_amount != 0){ // ถ้าชนะเดิมพัน ลบเงินที่ได้ออก
              var minus = (u.credit - b.get_amount)
              User.updateOne({_id:b.userid} , { $set: {credit:minus}},function(err){})
            }
          })          
        }
      }
    })

    Table.updateOne({ _id:req.body.tableid } , { $set: { notice : 2 }},function(err){})
    res.json({success:true})

  })
})

router.post('/cancel-yeekeebetall',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'cancel-yeekeebetall'
  })

  log.save(function(err){})

  Bet.find({
    tableid:req.body.tableid,
    round:req.body.round
  },function(err,bet){
    bet.forEach(b => {
      if(b.status != 2){
        Bet.updateOne({ _id:b._id } , { $set:{ status : 2 }},function(err){})
        
        if(b.bet_amount != 0){
          User.findOne({
            _id:b.userid
          },function(err,u){

            //รับเงินเดิมพันคืน
            User.updateOne({_id:b.userid} , { $inc: {credit:b.bet_amount}},function(err){})

            if(b.get_amount != 0){ // ถ้าชนะเดิมพัน ลบเงินที่ได้ออก
              var minus = (u.credit - b.get_amount)
              User.updateOne({_id:b.userid} , { $set: {credit:minus}},function(err){})
            }
          })          
        }
      }
    })

    Table.updateOne({ _id:req.body.tableid } , { $set: { notice : 2 }},function(err){})
    res.json({success:true})

  })
})

router.post('/new-notice',function(req,res){
  Bet.find({
    tableid:req.body.tableid
  },function(err,bet){

    var betList = []

    Bet.find({ 
      tableid:req.body.tableid 
    },function(err , bet){
      if(err) console.log(err)
      betArray = bet

      Table.updateOne({ _id:req.body.tableid },{ $set:{ open:0 , notice:1 }},function(err){})
      Result.deleteOne({ tableid:req.body.tableid },function(err){})

      bet.forEach(b => {
        if(b.get_amount > 0) User.updateOne({_id:b.userid},{$inc : { credit: - b.get_amount }},function(err){}) // เก็บเงินที่ถูกคืน
      })
      
      for (let i = 0; i < betArray.length; i++) {
        betList.push({'id':betArray[i]._id,'num':'','userid':''})
      }
  
      for (let a = 0; a < betArray.length; a++) {
        betList[a].num = betArray[a].num
        betList[a].userid = betArray[a].userid
      }
  
      betList.forEach(d => {
        d.num.forEach(n => {
          if(n.type == '3b'){
            n.number = ''
            n.status = 0
            n.get = 0
          }
  
          if (n.type == '3t') {
            n.number = ''
            n.status = 0
            n.get = 0
          }
  
          if (n.type == '3l') {
            n.number = ''
            n.status = 0
            n.get = 0
          }
  
          if (n.type == '2b') {
            n.number = ''
            n.status = 0
            n.get = 0
          }
  
          if (n.type == '2l') {
            n.number = ''
            n.status = 0
            n.get = 0
          }
  
          if (n.type == 'runb') {
            n.number = ''
            n.status = 0
            n.get = 0
          }
  
          if (n.type == 'runl') {
            n.number = ''
            n.status = 0
            n.get = 0
          }
        })
      })
  
      for (let i = 0; i < betList.length; i++) {
        Bet.updateOne({ _id : betList[i].id },{ $set: { timenotice:'', num : betList[i].num , status : 0 , get_amount:0 }}, function(err,res){
          if(err) console.log(err)
        })        
      }
    });
    res.json({success:true})
  })
  
})

router.post('/cancel-bet', function(req, res){
  var id = req.body.id

  Bet.findOne({ 
    _id:id 
  },function(err,bet){
    FullAll.findOne({
      lottery:bet.lottery
    },function(err,fullall){
      if(fullall){
        Table.findOne({
          _id:bet.tableid
        },function(err,table){
          var now = new Date().getTime()
          var timeremaining = diffTime(table.date_close,now)
          if(timeremaining >= fullall.timeCancel){
            if(bet.status == 0){
              User.findOne({
                _id:bet.userid
              },function(err,user){
                if(err) console.log(err)
                User.updateOne({ _id:user._id },{ $inc: { credit:bet.bet_amount }},function(err,res){
                  if(err) console.log(err)
                })

                var note = ""
                if(bet.lottery == 2){
                  note = 'รับเงินเดิมพันคืนจากการแทง ' + bet.title + ' รอบที่ ' + bet.round + ' / ' + new Date(bet.date).getDate() + " " + thmonth[new Date(bet.date).getMonth()] + " " + (new Date(bet.date).getFullYear() + 543)
                } else {
                  note = 'รับเงินเดิมพันคืนจากการแทง ' + bet.title + ' / ' + new Date(bet.date).getDate() + " " + thmonth[new Date(bet.date).getMonth()] + " " + (new Date(bet.date).getFullYear() + 543)
                }

                creditlog = new CreditLog({
                  detail:6,
                  by:'',
                  userid:user._id,
                  username:user.username,
                  credit:bet.bet_amount,
                  balance:(user.credit + bet.bet_amount),
                  time_create:new Date().getTime(),
                  betid:bet._id,
                  note:note,
                  mode:1
                })
                            
                creditlog.save(function(err){
                  if(err) console.log(err);
                })

                Bet.updateOne({ _id : id },{ $set: { status : 2 }}, function(err,res){
                  if(err) console.log(err)
                })

                res.json({success:true})
              })
            }
          } else {
            res.json({success:false})
          }
        })        
      }

    })
  })

});

router.get('/list-bet' , function(req, res) {
  var token = jwtDecode(getToken(req.headers))
  if(token){
    User.findOne({
      _id: token.id
    },function(err,user){
      if(err) console.log(err);
      if(!user){
        res.json({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        Bet.find({ userid: user._id } , function(err,bet){
          res.json({success: true , msg: bet})
        })
      }
    });
  } else {
    res.json({success: false, msg: 'Authentication failed.'});
  }
});

router.post('/list-bet' , function(req , res){
  Bet.find({ 
    tableid:req.body.tableid
  }).sort({lottery:-1}).exec(function(err , bet){
    if(err) console.log(err)
    res.json(bet);
  });
});

router.get('/list-betbyid/:id' , function(req, res) {
  var id = req.params.id
  Bet.findOne({ _id: id } , function(err,bet){ 
    res.json(bet)
  })
});

router.post('/list-yeekeebet' , function(req , res){
  Bet.find({ 
    round:req.body.round,
    tableid:req.body.tableid 
  },function(err , bet){
    if(err) console.log(err)
    res.json(bet);
  });
});

router.post('/list-betfromuser' , function(req , res){
  Bet.find({ 
    tableid:req.body.tableid,
    userid:req.body.userid
  },function(err , bet){
    if(err) console.log(err)
    res.json(bet);
  });
});

router.get('/list-thairesult',function(req,res){
  Result.findOne({
    lottery_id:1
  }).sort({table_timeopen:-1}).exec(function(err,result){
    res.json(result)
  })
})

router.post('/list-result',function(req,res){
  var daySelect = req.body.date
  var monthSelect = req.body.month
  var yearSelect = req.body.year

  var rsl = Array()

  if(daySelect && monthSelect && yearSelect){
    Result.find({
    },function(err,re){
      re.forEach(r => {
        if(r.lottery_id == 1){
          rsl.push(r)
          
        } else {
          var day = new Date(r.table_timeopen).getDate()
          var month = new Date(r.table_timeopen).getMonth() + 1
          var year = new Date(r.table_timeopen).getFullYear()

          if(day == daySelect && month == monthSelect && year == yearSelect){
            rsl.push(r)
          }
        }
      })
      res.json(rsl)
    })
  }
})

router.post('/yeekee-result' , function(req , res){
  var betArray = []
  var betList = []
  var betData = []
  var threet
  var runb = []
  var runl = []
  var threeb = req.body.threeb
  var threetI = req.body.threeb
  var twob
  var twol = req.body.twol

  var BetPay = []

  threet = unique(permutations(threetI))

  twob = threeb.substr(1,2)
  runb.push(threeb.substr(0,1))
  runb.push(threeb.substr(1,1))
  runb.push(threeb.substr(2,1))

  runl.push(twol.substr(0,1))
  runl.push(twol.substr(1,1))

  Bet.find({ 
    round:req.body.round,
    tableid:req.body.tableid
  },function(err , bet){
    if(err) console.log(err)
    betArray = bet

    Table.findOne({
      _id:req.body.tableid
    }).sort({_id:-1}).exec(function(err,table){
      YeeKee.findOne({
        round:req.body.round,
        tableid:table._id
      },function(err,yeekee){
        result = new Result({
          title_lottery:'หวยยี่กีรอบที่ ' + yeekee.round,
          lottery_id:2,
          tableid:table.tableid,
          table_timeopen:table.date_open,
          threeb:req.body.threeb,
          twol:req.body.twol,
          round:req.body.round,
          time_create:new Date().getTime()
        })
          
        result.save(function(err){
          if(err) console.log(err)
        })   
      })
    })

    
    for (let i = 0; i < betArray.length; i++) {
      betList.push({'id':betArray[i]._id,'num':'','userid':''})
      BetPay.push({'id':betArray[i]._id,'pay':0,'userid':betArray[i].userid,aff_userid:betArray[i].aff_userid,aff_amount:betArray[i].aff_amount,'status':betArray[i].status})
    }

    for (let a = 0; a < betArray.length; a++) {
      betList[a].num = betArray[a].num
      betList[a].userid = betArray[a].userid
    }

    betList.forEach(d => {
      d.num.forEach(n => {
        if(n.type == '3b'){
          if (n.num == req.body.threeb) {
            n.number = req.body.threeb
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = req.body.threeb
            n.status = 1
            n.get = 0
          }
        }

        if (n.type == '3t') {
          if (n.num == threet[0]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == threet[1]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == threet[2]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == threet[3]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == threet[4]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == threet[5]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = threet
            n.status = 1;
            n.get = 0
          }
        }

        if (n.type == '3l') {
          var threel = req.body.threel.split(',')
          var nn = threel.findIndex((value) => value == n.num)
          if(nn != -1){
            n.number = threel[nn]
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = req.body.threel
            n.status = 1
            n.get = 0
          }
        }

        if (n.type == '2b') {
          if (n.num == twob) {
            n.number = twob
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = twob
            n.status = 1
            n.get = 0
          }
        }

        if (n.type == '2l') {
          if (n.num == req.body.twol) {
            n.number = req.body.twol
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = req.body.twol
            n.status = 1
            n.get = 0
          }
        }

        if (n.type == 'runb') {
          if (n.num == runb[0]) {
            n.number = runb
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == runb[1]) {
            n.number = runb
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == runb[2]) {
            n.number = runb
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = runb
            n.status = 1
            n.get = 0
          }
        }

        if (n.type == 'runl') {
          if (n.num == runl[0]) {
            n.number = runl
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == runl[1]) {
            n.number = runl
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = runl
            n.status = 1
            n.get = 0
          }
        }
      })
    })

    var pay = 0;
    for (let i = 0; i < betList.length; i++) {
      Bet.updateOne({ _id : betList[i].id },{ $set: { timenotice:new Date().getTime() , num : betList[i].num , status : 1 }}, function(err,res){
        if(err) console.log(err)
      })

      for(let x of betList[i].num){
        BetPay[i].pay += x.get
      }
      
    }

    var userid = betArray.map(item => item.userid).filter((value, index, self) => self.indexOf(value) === index)
    var PriceALl = []

    for(i=0;i<=BetPay.length - 1;i++){
      var total = 0
      if(BetPay[i].status == 0){
        if(BetPay[i].aff_userid != "") User.updateOne({_id:BetPay[i].aff_userid },{ $inc: { income:BetPay[i].aff_amount }},function(err,res){})
        User.updateOne({ _id:BetPay[i].userid },{ $inc: { credit:BetPay[i].pay }},function(err,res){})
        Bet.updateOne({ _id:BetPay[i].id },{ $set: { get_amount: BetPay[i].pay }}, function(err,res){})

        var pay = BetPay[i].pay
        if(pay != 0){
          Bet.findOne({
            _id:BetPay[i].id
          },function(err,b){
            User.findOne({
              _id:b.userid
            },function(err,u){
              creditlog = new CreditLog({
                detail:4,
                by:'',
                userid:u._id,
                username:u.username,
                credit:pay,
                balance:u.credit,
                time_create:new Date().getTime(),
                betid:b._id,
                note:'ชนะเดิมพัน' + b.title + ' รอบที่ ' + b.round + ' / ' + new Date(b.date).getDate() + " " + thmonth[new Date(b.date).getMonth()] + " " + (new Date(b.date).getFullYear() + 543),
                mode:1
              })
            
              creditlog.save(function(err){
                if(err) console.log(err);
              })
            })
          })          
        }
      }
    }
  
  });

  res.json({success:true})
});

router.post('/find-num' , function(req , res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'find-num'
  })

  log.save(function(err){})

  var betArray = []
  var betList = []
  var betData = []
  var threet
  var runb = []
  var runl = []
  var threeb = req.body.threeb
  var threetI = req.body.threeb
  var twob
  var twol = req.body.twol

  var BetPay = []

  threet = unique(permutations(threetI))

  twob = threeb.substr(1,2)
  runb.push(threeb.substr(0,1))
  runb.push(threeb.substr(1,1))
  runb.push(threeb.substr(2,1))

  runl.push(twol.substr(0,1))
  runl.push(twol.substr(1,1))

  Bet.find({ 
    tableid:req.body.tableid 
  },function(err , bet){
    if(err) console.log(err)
    betArray = bet

    Table.updateOne({_id:req.body.tableid},{$set:{notice:2}},function(err){
      if(err) console.log(err)
    })

    Table.findOne({
      _id:req.body.tableid
    },function(err,table){
      if(table.lottery == 1){
        result = new Result({
          title_lottery:table.title,
          lottery_id:table.lottery,
          tableid:table.id,
          table_timeopen:table.date_close,
          lottery:req.body.lottery_1st,
          threel:req.body.threel,
          twol:req.body.twol,
          time_create:new Date().getTime()
        })
          
        result.save(function(err){
          if(err) console.log(err)
        })
      } else {
        result = new Result({
          title_lottery:table.title,
          lottery_id:table.lottery,
          tableid:table.id,
          table_timeopen:table.date_open,
          threeb:req.body.threeb,
          twol:req.body.twol,
          time_create:new Date().getTime()
        })
          
        result.save(function(err){
          if(err) console.log(err)
        })
      }      
    })
    
    for (let i = 0; i < betArray.length; i++) {
      betList.push({'id':betArray[i]._id,'num':'','userid':''})
      BetPay.push({'id':betArray[i]._id,'pay':0,'userid':betArray[i].userid,aff_userid:betArray[i].aff_userid,aff_amount:betArray[i].aff_amount,'status':betArray[i].status})
    }

    for (let a = 0; a < betArray.length; a++) {
      betList[a].num = betArray[a].num
      betList[a].userid = betArray[a].userid
    }

    betList.forEach(d => {
      d.num.forEach(n => {
        if(n.type == '3b'){
          if (n.num == req.body.threeb) {
            n.number = req.body.threeb
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = req.body.threeb
            n.status = 1
            n.get = 0
          }
        }

        if (n.type == '3t') {
          if (n.num == threet[0]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == threet[1]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == threet[2]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == threet[3]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == threet[4]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == threet[5]) {
            n.number = threet
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = threet
            n.status = 1;
            n.get = 0
          }
        }

        if (n.type == '3l') {
          var threel = req.body.threel.split(',')
          var nn = threel.findIndex((value) => value == n.num)
          if(nn != -1){
            n.number = threel[nn]
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = req.body.threel
            n.status = 1
            n.get = 0
          }
        }

        if (n.type == '2b') {
          if (n.num == twob) {
            n.number = twob
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = twob
            n.status = 1
            n.get = 0
          }
        }

        if (n.type == '2l') {
          if (n.num == req.body.twol) {
            n.number = req.body.twol
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = req.body.twol
            n.status = 1
            n.get = 0
          }
        }

        if (n.type == 'runb') {
          if (n.num == runb[0]) {
            n.number = runb
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == runb[1]) {
            n.number = runb
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == runb[2]) {
            n.number = runb
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = runb
            n.status = 1
            n.get = 0
          }
        }

        if (n.type == 'runl') {
          if (n.num == runl[0]) {
            n.number = runl
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else if (n.num == runl[1]) {
            n.number = runl
            n.status = 1
            n.get = parseInt(n.pay * n.price)
          } else {
            n.number = runl
            n.status = 1
            n.get = 0
          }
        }
      })
    })

    var pay = 0;
    for (let i = 0; i < betList.length; i++) {
      Bet.updateOne({ _id : betList[i].id },{ $set: { timenotice:new Date().getTime() , num : betList[i].num , status : 1 }}, function(err,res){
        if(err) console.log(err)
      })

      for(let x of betList[i].num){
        BetPay[i].pay += x.get
      }
      
    }

    var userid = betArray.map(item => item.userid).filter((value, index, self) => self.indexOf(value) === index)
    var PriceALl = []

    for(i=0;i<=BetPay.length - 1;i++){
      var total = 0
      if(BetPay[i].status == 0){
        if(BetPay[i].aff_userid != "") User.updateOne({_id:BetPay[i].aff_userid },{ $inc: { income:BetPay[i].aff_amount }},function(err,res){})
        User.updateOne({ _id:BetPay[i].userid },{ $inc: { credit:BetPay[i].pay }},function(err,res){})
        Bet.updateOne({ _id:BetPay[i].id },{ $set: { get_amount: BetPay[i].pay }}, function(err,res){})

        var pay = BetPay[i].pay
        if(pay != 0){
          Bet.findOne({
            _id:BetPay[i].id
          },function(err,b){
            User.findOne({
              _id:b.userid
            },function(err,u){
              creditlog = new CreditLog({
                detail:4,
                by:'',
                userid:u._id,
                username:u.username,
                credit:pay,
                balance:u.credit,
                time_create:new Date().getTime(),
                betid:b._id,
                note:'ชนะเดิมพัน' + b.title + ' / ' + new Date(b.date).getDate() + " " + thmonth[new Date(b.date).getMonth()] + " " + (new Date(b.date).getFullYear() + 543),
                mode:1
              })
            
              creditlog.save(function(err){
                if(err) console.log(err);
              })
            })
          })          
        }
      }
    }
  });

  res.json({success:true})
});

router.get('/list-betid', function(req, res) {
  var token = jwtDecode(getToken(req.headers))
  if(token){
    User.findOne({
      _id: token.id
    },function(err,user){
      if(err) throw err;
        
      if(!user){
        res.json({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        Bet.findOne({ userid: user._id , _id: req.headers.id } , function(err,bet){ 
          res.json({success: true , msg: bet})
        })
      }
    });
  } else {
    res.json({success: false, msg: 'Authentication failed.'});
  }
});

router.post('/lottery-open',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'lottery-open'
  })

  log.save(function(err){})

  ListLottery.findOne({
    id:req.body.id
  },function(err,list){
    if(err) throw err;

    if(!list){
      res.json({success: false, msg: 'ไม่พบข้อมูลของหวยไอดีนี้ '});
    } else {
      ListLottery.updateOne({ id : req.body.id },{ $set: { open : 'true' }}, function(err,res){
        console.log(err + res);
      });

      res.json({success: true, msg: 'Successful Open Lottery'});      
    }

  });
});

router.post('/lottery-close',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'lottery-close'
  })

  log.save(function(err){})

  ListLottery.findOne({
    id:req.body.id
  },function(err,list){
    if(err) throw err;

    if(!list){
      res.json({success: false, msg: 'ไม่พบข้อมูลของหวยไอดีนี้ '});
    } else {
      ListLottery.updateOne({ id : req.body.id },{ $set: { open : 'false' }}, function(err,res){
        console.log(err + res);
      });

      res.json({success: true, msg: 'Successful Open Lottery'});      
    }
  });
});

router.post('/create-bank',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'create-bank'
  })

  log.save(function(err){})

  var bank = new Bank({
    id:req.body.id,
    bank:req.body.bank,
    bankno:req.body.bankno,
    bankname:req.body.bankname
  });

  bank.save(function(err) {
    if (err) res.json({success: false, msg: err});
    res.json({success: true, msg: 'เพิ่มธนาคารเรียบร้อยแล้ว'});
  });
});

router.post('/edit-bank',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'edit-bank'
  })

  log.save(function(err){})

  Bank.updateOne({ id: req.body.id } , { $set: { bank : req.body.bank , bankno : req.body.bankno , bankname : req.body.bankname }},function(err,res){
    if(err) res.json({ success: false })
  });

  res.json({ success: true })
})

router.post('/delete-bank',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'delete-bank'
  })

  log.save(function(err){})

  Bank.deleteOne({ id:req.body.id },function(err,re){
    if(err) res.json({ success:false })
    res.json({success:true})
  })
})

router.get('/list-bank',function(req,res){
  Bank.find().sort({id:1}).exec(function (err, data) {
    if (err) console.log(err)
    res.json(data);
  });
});

router.get('/list-bank/:id',function(req,res){
  Bank.findOne({
    id:req.params.id
  },function (err, data) {
    if (err) console.log(err)
    res.json(data);
  });
});

router.post('/transaction-notification',function(req,res){
  Transaction.updateOne({_id:req.body.id},{ $set:{ notification:1 }},function(err){
    if(err) console.log(err)
    res.json({success:true})
  })
})

router.post('/deposit',function(req,res){
  try {
    if (!req.body.bank || !req.body.amount || !req.body.hour || !req.body.min || !req.body.id) {
      res.json({success: false, msg: 'กรุณากรอกข้อมูลให้ครบถ้วน'});
    } else {
      Settings.findOne({

      },function(err,settings){
        if(req.body.amount >= settings.deposit_min){
          Bank.findOne({
            id:req.body.bank
          },function(err, bank){
            var deposit = new Transaction({
              bank: bank.bank,
              bankno: bank.bankno,
              bankname: bank.bankname,
              amount: req.body.amount,
              nowtrans: new Date().getTime(),
              timedeposit: req.body.hour + ":" + req.body.min,
              note: req.body.note,
              userid: req.body.id,
              username: req.body.username,
              type:1, // ประเภท ฝากเงิน
              notification:0, //ยังไม่ได้แจ้งเตือน
              status:0
            });

            deposit.save(function(err) {
              if (err) return res.json({success: false, msg: err});
              res.json({success: true, msg: 'แจ้งฝากเครดิตเรียบร้อยแล้ว'});
            });
          });  
        } else {
          res.json({success:false , msg : 'จำนวนที่ต้องการฝากต่ำกว่าจำนวนฝากขั้นต่ำ'})
        }
        
      })


    }
  } catch (error) {
    console.log(error);
  }
});

router.post('/withdraw',function(req,res){
  try {
    if (!req.body.bank || !req.body.amount || !req.body.id) {
      res.json({success: false, msg: 'กรุณากรอกข้อมูลให้ครบถ้วน'});
    } else {
      Settings.findOne({

      },function(err,settings){
        if(req.body.amount > settings.withdraw_min){
          var withdraw = new Transaction({
            bank: req.body.bank,
            bankno: req.body.bankno,
            bankname: req.body.bankname,
            amount: req.body.amount,
            nowtrans: new Date().getTime(),
            note: req.body.note,
            userid: req.body.id,
            username: req.body.username,
            type:2, // ประเภท ฝากเงิน
            notification:0, //ยังไม่ได้แจ้งเตือน
            status:0
          });

          User.findOne({ _id: req.body.id },function(err,user){
            if(user.credit >= req.body.amount){
              var credit = user.credit - req.body.amount;

              User.updateOne({ _id : user._id },{ $set: { credit : credit }}, function(err,res){
                if(err) console.log(err)
              });

              withdraw.save(function(err) {
                if (err) return res.json({success: false, msg: err});
                res.json({success: true, msg: 'แจ้งถอนเครดิตเรียบร้อยแล้ว'});
              });
            } 
            
            if(user.credit < req.body.amount){
              res.json({success:false, msg:'จำนวนเครดิตไม่เพียงพอที่จะทำการถอน'});
            }
          });
        } else {
          res.json({success:false , msg:'จำนวนที่ต้องการถอนต่ำกว่าจำนวนถอนขั้นต่ำ'})
        }
      })
    }
  } catch (error) {
    console.log(error);
  }
});

router.get('/transaction' , function(req, res) {
  var token = jwtDecode(getToken(req.headers))
  if (token) {
    User.findOne({
      _id: token.id
    },function(err,user){
      if(err) console.log(err)

      if(!user){
        res.json({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        Transaction.find({ userid: user._id }).sort({_id:-1}).exec(function (err, data) {
          if (err) console.log(err)
          res.json({ success: true, msg: data});
        });
      }
    });
    
  } else {
    res.json({success: false, msg: 'Authentication failed.'});
  }
});

router.get('/list-information/:id',function(req,res){
  Information.findOne({
    _id:req.params.id
  },function(err,information){
    res.json(information)
  })
})

router.get('/list-information',function(req,res){
  Information.find({

  }).sort({time_created:-1}).exec(function(err,information){
    res.json(information)
  })
})

router.post('/edit-information', function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'edit-information'
  })

  log.save(function(err){})
  Information.updateOne({ _id:req.body.id } , { $set : { title: req.body.title , body: req.body.body , createby:req.body.username }},function(err,res){
    if(err) console.log(err)
  })

  res.json({success:true})
})

router.post('/create-information', function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'create-information'
  })

  log.save(function(err){})
  var information = new Information({
    title:req.body.title,
    body:req.body.body,
    time_created:new Date().getTime(),
    createby:req.body.username
  })

  information.save(function(err){})
  res.json({success:true})
})

router.post('/delete-information', function(req,res){
  Information.deleteOne({ _id:req.body.id } , function(err){})
  res.json({success:true})
})

router.get('/lastdeposit', function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    var userid = jwtDecode(token)
    User.findOne({
      _id: userid.id
    },function(err,user){
      if(err) throw err;

      if(!user){
        res.json({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        Transaction.findOne({ userid: user._id }).sort({ _id:-1}).exec(function (err, data) {
          if (err) console.log(err)
          if(data){
            res.json({ success: true, msg: data});  
          } else {
            res.json({ success: false});  
          }
          
        });
      }
    });
    
  } else {
    res.json({success: false, msg: 'Authentication failed.'});
  }
});

router.get('/list-transactions', function(req, res) {
  Transaction.find({

  }).sort({nowtrans:-1}).exec(function (err, data) {
    if (err) console.log(err)
    res.json(data)
  });
});

router.get('/control-transactions', function(req, res) {
  var deposit = 0,withdraw = 0,income = 0
  Transaction.find({

  }).sort({nowtrans:-1}).exec(function (err, data) {
    if (err) console.log(err)
    data.forEach(element => {
      if(element.type == 1 && element.status == 0) deposit++
      if(element.type == 2 && element.status == 0) withdraw++
      if(element.type == 3 && element.status == 0) income++
    });
    res.json({list:data,depositp:deposit,withdrawp:withdraw,incomep:income});
  });
});

router.post('/list-transactionsfromusername', function(req, res) {
  Transaction.find({
    username:req.body.username
  }).sort({nowtrans:-1}).exec(function (err, data) {
    if (err) console.log(err)
    res.json(data);
  });
});

router.get('/list-lasttransactions', function(req, res) {
  Transaction.findOne({

  }).sort({nowtrans:-1}).exec(function (err, data) {
    if (err) console.log(err)
    res.json(data);
  });
});

router.post('/approve-trans', function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'approve-trans'
  })

  log.save(function(err){})
  Transaction.findOne({ _id:req.body.id },function (err ,data){  
    try {
      if(data){
        if(data.type == 1){
          User.findOne({ _id:data.userid },function(err,user){

            User.updateOne({ _id : user._id },{ $inc: { credit : data.amount }}, function(err,res){
              if(err) console.log(err);
            });

            Transaction.updateOne({ _id: data._id },{ $set: { status : 1 }} , function(err,res){
              if(err) console.log(err);
            });

            creditlog = new CreditLog({
              detail:1,
              by:req.body.username,
              userid:user._id,
              username:user.username,
              credit:data.amount,
              balance:(user.credit + data.amount),
              time_create:new Date().getTime(),
              betid:'',
              note:'',
              mode:1
            })

            creditlog.save(function(err){
              if(err) console.log(err); else res.json({success: true, msg: "ทำรายการเรียบร้อยแล้ว"});
            })
          });
        }

        if(data.type == 2){
          User.findOne({ _id:data.userid },function(err,user){
            Transaction.updateOne({ _id: data._id },{ $set: { status : 1 }} , function(err,res){
              console.log(err + res);
            });

            creditlog = new CreditLog({
              detail:2,
              by:req.body.username,
              userid:user._id,
              username:user.username,
              credit:data.amount,
              balance:user.credit,
              time_create:new Date().getTime(),
              betid:'',
              note:'',
              mode:2
            })

            creditlog.save(function(err){
              if(err) console.log(err); else res.json({success: true, msg: "ทำรายการเรียบร้อยแล้ว"});
            })
          });
        }
        
        if(data.type == 3){
          User.findOne({ _id:data.userid },function(err,user){
            Transaction.updateOne({ _id: data._id },{ $set: { status : 1 }} , function(err,res){
              console.log(err + res);
            });

            User.updateOne({ _id:user._id},{ $inc : { credit:data.amount }} , function(err,res){ })

            res.json({success: true, msg: "ทำรายการเรียบร้อยแล้ว"});
          });
        }
        
      } else {
        res.json({success: false, msg: "ไม่สามารถทำรายการได้"});
      } 

    } catch (error) {
      res.json({success: false, msg: error});
    }
  });
});

router.post('/cancel-trans',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'cancel-trans'
  })

  log.save(function(err){})

  try {
    Transaction.findOne({ _id:req.body.id },function (err ,data){ 
      if(err) res.json({success: false, msg: "ไม่สามารถทำรายการได้"})
      if(data){
        if(data.type == 1){
          User.findOne({ _id:data.userid },function(err,user){

            Transaction.updateOne({ _id: data._id },{ $set: { status : 2 }} , function(err,res){
              console.log(err + res);
            });

            res.json({success: true, msg: "ยกเลิกทำรายการ"});
          });
        }

        if(data.type == 2){
          User.findOne({ _id:data.userid },function(err,user){

            var credit = user.credit + data.amount;
            User.updateOne({ _id : user._id },{ $set: { credit : credit }}, function(err,res){
              console.log(err + res);
            });

            Transaction.updateOne({ _id: data._id },{ $set: { status : 2 }} , function(err,res){
              console.log(err + res);
            });

            res.json({success: true, msg: "ยกเลิกทำรายการ"});
          });
        }

        if(data.type == 3){
          User.findOne({ _id:data.userid },function(err,user){

            var credit = user.credit - data.amount;

            Transaction.updateOne({ _id: data._id },{ $set: { status : 2 }} , function(err,res){
              console.log(err + res);
            });

            User.updateOne({ _id:user._id},{ $set : { credit:credit }} , function(err,res){ })
            User.updateOne({ _id:user._id},{ $inc : { income:data.amount }} , function(err,res){ })

            res.json({success: true, msg: "ยกเลิกทำรายการ"});
          });
        }
        
      } else {
        res.json({success: false, msg: "ไม่สามารถทำรายการได้"});
      } 
    });
  } catch (error) {
    res.json({success: false, msg: error});
  }
});

router.get('/lasttable',function(req,res){
  Table.findOne({
  }).sort({id: -1}).exec(function(err, table) {
    res.json(table)
  });
})

router.post('/create-table',function(req,res){
  var dateOpen = req.body.date_open + " " + req.body.hour_open + ":" + req.body.min_open
  var dateClose = req.body.date_close + " " + req.body.hour_close + ":" + req.body.min_close

  ListLottery.findOne({
    id:req.body.lottery
  },function(err,lottery){
    if(err) console.log(err)

    var table = new Table({
      lottery:lottery.id,
      title:lottery.title,
      group:lottery.group,
      open:req.body.open,
      date_open: new Date(dateOpen).getTime(),
      date_close: new Date(dateClose).getTime(),
      open:0,
      notice:0
    });

    table.save(function(err){
      if(err) console.log(err);
      
      res.json({success:true});
    });
  });
});

router.post('/edit-table',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'edit-table'
  })

  log.save(function(err){})

  var id = req.body.id;
  var date_open = new Date(req.body.dateOpen).getTime();
  var date_close = new Date(req.body.dateClose).getTime();

  Table.updateOne({ _id : id } , { $set : { date_open : date_open , date_close : date_close }} , function(err,res){
    if(err) console.log(err);
  });

  res.json({success:true});
});

router.post('/open-table',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'open-table'
  })

  log.save(function(err){})

  var id = req.body.id
  var open = req.body.open

  Table.updateOne({ _id:id } , { $set : { open:open }}, function(err,res){
    if(err) console.log(err);
  });

  res.json({success:true});
});

router.get('/list-table',function(req,res){
  Table.find({}).sort({date_close:1}).exec(function(err,data){
    if(err) console.log(err)
    res.json(data);
  });
});

router.get('/list-table/:id', function(req,res){
  if(req.params.id.length <= 2){
    ListLottery.findOne({
      id:req.params.id
    }).sort({_id:-1}).exec(function(err,lottery){
      Table.findOne({
        lottery: lottery.id
      }).sort({_id:-1}).exec(function(err,table){
        if(err) console.log(err)
        res.json(table);
      });    
    })    
  } else {
    Table.findOne({
      _id: req.params.id
    }).sort({_id:-1}).exec(function(err,table){
      if(err) console.log(err)
      res.json(table);
    });     
  }
});


function Tablefromlottery(id) {
  return new Promise(async (resolve) => {
      let data = await Table.findOne({
          lottery: id
      }).sort({_id:-1})
      resolve(data)
  })
}

router.post('/list-tableslottery', async(req, res) => {
  let lottery = req.body.lottery
  let lotteryTables = []
  for(let x of lottery){
      let d = await Tablefromlottery(x)
      lotteryTables.push(d)
  }

  res.json(lotteryTables)
})

router.get('/list-tablefromlottery/:id', async(req, res) => {
  Table.findOne({
    lottery: req.params.id
  }).sort({_id: -1}).exec(function(err, table) {
    if(err) console.log(err)
    res.json(table)
  });
})

router.post('/list-tableformid',function(req,res){
  Table.find({
    lottery: req.body.lottery
  },function(err,table){
    if(err) console.log(err)
    res.json(table);
  });
});

router.post('/update-table',function(req,res){
  Table.find({

  },function(err,data){
    data.forEach(d => {
      Table.updateOne({ _id:d._id } , { $set: { notice:0 }},function(err,res){
        if(err) console.log(err)
      })
    })
  })

});

router.post('/delete-table',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'delete-table'
  })

  log.save(function(err){})

  var id = req.body.id;
  
  Table.deleteOne({ _id : id },function(err,res){
    console.log(err + res);
  });

  res.json({success:true,msg:'delete Table Success'});
});

router.get('/list-member',function(req, res){
  User.find(function(err,data){
    if(err) console.log(err)
    user = [];

    data.forEach(element => {
      user.push({ '_id':element._id,'username':element.username,'email':element.email , 'date_register':element.date_register ,'bank':element.bank , 'bankno':element.bankno , 'bankname':element.bankname ,'telephone': element.telephone ,'credit':element.credit , 'ip':element.ip , 'status':element.status , 'income':element.income , 'aff_userid':element.aff_userid , 'last_login':element.last_login} );
    });
    
    res.json(user);
  });
});

router.post('/listtotalbet-member',function(req,res){
  betAmount = []
  Bet.find({ userid: req.body.id} , function(err,bet){
    bet.forEach(b => {
      betAmount.push({id:b.userid,bet_amount:b.bet_amount})
    })
  
    var total = 0
    betAmount.forEach(ba => {
      total += ba.bet_amount
    })

    res.json({userid:req.body.id,total:total})
  })
})

router.post('/list-member',function(req, res){
  User.find({
    _id:req.body.id
  },function(err,data){
    if(err) console.log(err)
    res.json(data);
  });
});

router.post('/list-memberfromsearch',function(req, res){

  if(req.body.cmd == "ชื่อผู้ใช้"){
    var user = []
    var regexp = new RegExp("^"+ req.body.data);
    User.findOne({
      username:regexp
    },function(err,data){
      if(err) console.log(err)
      res.json(data);
    });  
  }
  if(req.body.cmd == "ชื่อนามสกุล"){
    var regexp = new RegExp("^"+ req.body.data);
    User.findOne({
      bankname:regexp
    },function(err,data){
      if(err) console.log(err)
      res.json(data);
    });  
  }
  if(req.body.cmd == "เลขบัญชี"){
    var regexp = new RegExp("^"+ req.body.data);
    User.findOne({
      bankno:regexp
    },function(err,data){
      if(err) console.log(err)
      res.json(data);
    });  
  }
  if(req.body.cmd == "เบอร์โทรศัพท์"){
    var regexp = new RegExp("^"+ req.body.data);
    User.findOne({
      telephone:regexp
    },function(err,data){
      if(err) console.log(err)
      res.json(data);
    });  
  }
  
});

router.post('/edit-member',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'edit-member'
  })

  log.save(function(err){
    if(err) console.log(err)
  })

  var id = req.body.id;
  User.updateOne({ _id : id } , { $set : { email : req.body.email , bank : req.body.bank , bankno : req.body.bankno , bankname : req.body.bankname , telephone : req.body.telephone , credit : req.body.credit , token : req.body.token }} , function(err,res){
    if(err) console.log(err)
  });
  res.json({ success:true })
});

router.post('/delete-member',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'delete-member'
  })

  log.save(function(err){})

  var id = req.body.id;
  User.deleteOne({_id: id } , function(err, res){
    if(err) console.log(err)
  })

  res.json({ success:true })
});

router.post('/block-member',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'block-member'
  })

  log.save(function(err){})

  var id = req.body.id;
  User.updateOne({ _id:id } , { $set : {status : 1}} , function(err,res){
    if(err) console.log(err)
  })
  res.json({ success:true })
});

router.post('/unblock-member',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'unblock-member'
  })

  log.save(function(err){})

  var id = req.body.id;
  User.updateOne({ _id:id } , { $set : {status : 0}} , function(err,res){
    if(err) console.log(err)
  })
  res.json({ success:true })
});

router.post('/listbet-member',function(req,res){
  var id = req.body.id;
  Bet.find({ userid: id } , function(err,bet){
    res.json(bet)
  })
  
});

router.post('/create-full',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'create-full'
  })

  log.save(function(err){})

  var full = new Full({
    number: req.body.number,
    type: req.body.type,
    lottery: req.body.lottery,
    pay: req.body.pay
  });

  full.save(function(err) {
    if(err) { 
      res.json({ success: false }); 
    } else {
      res.json({ success: true });  
    }
  });
});

router.post('/create-closed',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'create-closed'
  })

  log.save(function(err){})

  var closed = new Closed({
    number: req.body.number,
    type: req.body.type,
    lottery: req.body.lottery,
    amount: req.body.amount
  });

  closed.save(function(err) {
    if(err) { 
      res.json({ success: false }); 
    } else {
      res.json({ success: true });  
    }
  });
});

router.post('/delete-closed',function(req,res){
  Closed.deleteOne({ _id: req.body.id } , function(err){
    if(err) console.log(err); else res.json({success:true})
  })
});

router.post('/save-fullmember',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'save-fullmember'
  })

  log.save(function(err){})

  User.findOne({
    _id:req.body.userid
  },function(err,user){

    FullMember.findOne({
      userid:user._id,
      lottery:req.body.lottery
    },function(err,data){
      if(data){
        if(data.lottery == 1 || data.lottery == 30 || data.lottery == 31){
          
          FullMember.updateOne({ userid:user._id , lottery:req.body.lottery },{ $set:{
            threeBFull:req.body.threeBFull,
            threeTFull:req.body.threeTFull,
            threeLFull:req.body.threeLFull,
            twoBFull:req.body.twoBFull,
            twoLFull:req.body.twoLFull,
            runBFull:req.body.runBFull,
            runLFull:req.body.runLFull,
            threeBMaximum:req.body.threeBMaximum,
            threeTMaximum:req.body.threeTMaximum,
            threeLMaximum:req.body.threeLMaximum,
            twoBMaximum:req.body.twoBMaximum,
            twoLMaximum:req.body.twoLMaximum,
            runBMaximum:req.body.runBMaximum,
            runLMaximum:req.body.runLMaximum,
            threeBFullPay:req.body.threeBFullPay,
            threeTFullPay:req.body.threeTFullPay,
            threeLFullPay:req.body.threeLFullPay,
            twoBFullPay:req.body.twoBFullPay,
            twoLFullPay:req.body.twoLFullPay,
            runBFullPay:req.body.runBFullPay,
            runLFullPay:req.body.runLFullPay,
          }},function(err){
            if(err){
              console.log(err)
              res.json({ success: false })
            } else {
              res.json({ success: true })
            }
          })          
        } else {
          FullMember.updateOne({ userid:user._id , lottery:req.body.lottery },{ $set:{
            threeBFull:req.body.threeBFull,
            threeTFull:req.body.threeTFull,
            twoBFull:req.body.twoBFull,
            twoLFull:req.body.twoLFull,
            runBFull:req.body.runBFull,
            runLFull:req.body.runLFull,
            threeBMaximum:req.body.threeBMaximum,
            threeTMaximum:req.body.threeTMaximum,
            twoBMaximum:req.body.twoBMaximum,
            twoLMaximum:req.body.twoLMaximum,
            runBMaximum:req.body.runBMaximum,
            runLMaximum:req.body.runLMaximum,
            threeBFullPay:req.body.threeBFullPay,
            threeTFullPay:req.body.threeTFullPay,
            twoBFullPay:req.body.twoBFullPay,
            twoLFullPay:req.body.twoLFullPay,
            runBFullPay:req.body.runBFullPay,
            runLFullPay:req.body.runLFullPay,
          }},function(err){
            if(err){
              res.json({ success: false })
            } else {
              res.json({ success: true })
            }
          })     
        }

      } else {
        
        var fullmember = new FullMember({
          lottery: req.body.lottery,
          userid:req.body.userid,
          username:user.username,
          threeBFull:req.body.threeBFull,
          threeTFull:req.body.threeTFull,
          threeLFull:req.body.threeLFull,
          twoBFull:req.body.twoBFull,
          twoLFull:req.body.twoLFull,
          runBFull:req.body.runBFull,
          runLFull:req.body.runLFull,
          threeBMaximum:req.body.threeBMaximum,
          threeTMaximum:req.body.threeTMaximum,
          threeLMaximum:req.body.threeLMaximum,
          twoBMaximum:req.body.twoBMaximum,
          twoLMaximum:req.body.twoLMaximum,
          runBMaximum:req.body.runBMaximum,
          runLMaximum:req.body.runLMaximum,
          threeBFullPay:req.body.threeBFullPay,
          threeTFullPay:req.body.threeTFullPay,
          threeLFullPay:req.body.threeLFullPay,
          twoBFullPay:req.body.twoBFullPay,
          twoLFullPay:req.body.twoLFullPay,
          runBFullPay:req.body.runBFullPay,
          runLFullPay:req.body.runLFullPay,
        });

        fullmember.save(function(err){
          if(err){
            res.json({ success: false })
          } else {
            res.json({ success: true })
          }
        });        
      }
    })
  })

});

router.post('/list-fullmembers',function(req, res){
  FullMember.find({
    lottery:req.body.lottery
  },function(err,full){
    if(full) res.json(full); else res.json({success:false})
  })
});

router.post('/list-closed',function(req, res){
  Closed.find({
    lottery:req.body.lottery
  },function(err,closed){
    if(closed) res.json(closed); else res.json({success:false})
  })
});

router.post('/list-fullmembersfromuser',function(req, res){
  FullMember.findOne({
    lottery:req.body.lottery,
    userid:req.body.userid
  },function(err,fullmember){
    if(fullmember) res.json(fullmember); else res.json({success:false})
  })
});

router.post('/list-fullmember',function(req, res){
  FullMember.findOne({
    userid:req.body.userid,
    lottery:req.body.lottery
  },function(err,full){
    if(full) res.json(full); else res.json({success:false})
  })
});

router.post('/delete-fullmember',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'delete-fullmember'
  })

  log.save(function(err){})

  FullMember.deleteOne({ _id: req.body.id } , function(err, res){
    if(err) console.log(err)
  })

  res.json({success: true})
})

router.post('/list-fullmember/:tableid',function(req, res){
  FullMember.find({
    tableid:req.params.tableid
  },function(err,full){
    if(full) res.json(full); res.json({success:false})
  })
});

router.post('/list-full',function(req, res){
  Full.find({ 
    lottery:req.body.lottery
  },function(err,data){
    if(err) console.log(err)
    res.json(data);
  });
});

router.post('/list-fullall',function(req, res){
  FullAll.findOne({ 
    lottery: req.body.lottery 
  }).sort({ _id:-1}).exec(function (err, data) {
    if (err) console.log(err)
    if(data){
      res.json(data);  
    } else {
      res.json({'success':false});  
    }
  });
});

router.post('/last-fullall',function(req,res){
  FullAll.findOne({
    lottery:req.body.lottery
  }).sort({_id:-1}).exec( function(err,data){
    res.json(data)
  })  
})

router.post('/save-fullall',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'save-fullall'
  })

  log.save(function(err){})

  var a = Array()
  FullAll.findOne({ 
    lottery: req.body.lottery
  }).sort({ _id: -1}).exec(function (err, data) {
    if (err) console.log(err)
    if(data){
      if(req.body.lottery == 1 || req.body.lottery == 30 || req.body.lottery == 31){
        FullAll.updateOne({ lottery:req.body.lottery } , { $set : {
          threeBFull:req.body.threeBFull,
          threeTFull:req.body.threeTFull,
          threeLFull:req.body.threeLFull,
          twoBFull:req.body.twoBFull,
          twoLFull:req.body.twoLFull,
          runBFull:req.body.runBFull,
          runLFull:req.body.runLFull,
          threeBMaximum:req.body.threeBMaximum,
          threeTMaximum:req.body.threeTMaximum,
          threeLMaximum:req.body.threeLMaximum,
          twoBMaximum:req.body.twoBMaximum,
          twoLMaximum:req.body.twoLMaximum,
          runBMaximum:req.body.runBMaximum,
          runLMaximum:req.body.runLMaximum,
          threeBFullPay:req.body.threeBFullPay,
          threeTFullPay:req.body.threeTFullPay,
          twoBFullPay:req.body.twoBFullPay,
          twoLFullPay:req.body.twoLFullPay,
          runBFullPay:req.body.runBFullPay,
          runLFullPay:req.body.runLFullPay,
          lottery:req.body.lottery,
          timeCancel: req.body.timeCancel
        }} , function(err,res){
          if(err) console.log(err)
        }) 
      } else {
        FullAll.updateOne({ lottery:req.body.lottery } , { $set : {
          threeBFull:req.body.threeBFull,
          threeTFull:req.body.threeTFull,
          twoBFull:req.body.twoBFull,
          twoLFull:req.body.twoLFull,
          runBFull:req.body.runBFull,
          runLFull:req.body.runLFull,
          threeBMaximum:req.body.threeBMaximum,
          threeTMaximum:req.body.threeTMaximum,
          twoBMaximum:req.body.twoBMaximum,
          twoLMaximum:req.body.twoLMaximum,
          runBMaximum:req.body.runBMaximum,
          runLMaximum:req.body.runLMaximum,
          threeBFullPay:req.body.threeBFullPay,
          threeTFullPay:req.body.threeTFullPay,
          twoBFullPay:req.body.twoBFullPay,
          twoLFullPay:req.body.twoLFullPay,
          runBFullPay:req.body.runBFullPay,
          runLFullPay:req.body.runLFullPay,
          lottery:req.body.lottery,
          timeCancel: req.body.timeCancel
        }} , function(err,res){
          if(err) console.log(err)
        }) 
      }      
    } else {
      if(req.body.lottery == 1){
        var fullall = new FullAll({
          lottery: req.body.lottery,
          threeBFull: req.body.threeBFull,
          threeTFull: req.body.threeTFull,
          threeLFull: req.body.threeLFull,
          twoBFull: req.body.twoBFull,
          twoLFull: req.body.twoLFull,
          runBFull: req.body.runBFull,
          runLFull: req.body.runLFull,
          threeBMaximum: req.body.threeBMaximum,
          threeTMaximum: req.body.threeTMaximum,
          threeLMaximum: req.body.threeLMaximum,
          twoBMaximum: req.body.twoBMaximum,
          twoLMaximum: req.body.twoLMaximum,
          runBMaximum: req.body.runBMaximum,
          runLMaximum: req.body.runLMaximum,
          threeBFullPay: req.body.threeBFullPay,
          threeTFullPay: req.body.threeTFullPay,
          threeLFullPay: req.body.threeLFullPay,
          twoBFullPay: req.body.twoBFullPay,
          twoLFullPay: req.body.twoLFullPay,
          runBFullPay: req.body.runBFullPay,
          runLFullPay: req.body.runLFullPay,
          timeCancel: req.body.timeCancel
        });
      
        fullall.save(function(err){
          if(err){
            res.json({ success: false })
          } else {
            res.json({ success: true })
          }
        });
      } else {
        var fullall = new FullAll({
          lottery: req.body.lottery,
          threeBFull: req.body.threeBFull,
          threeTFull: req.body.threeTFull,
          twoBFull: req.body.twoBFull,
          twoLFull: req.body.twoLFull,
          runBFull: req.body.runBFull,
          runLFull: req.body.runLFull,
          threeBMaximum: req.body.threeBMaximum,
          threeTMaximum: req.body.threeTMaximum,
          twoBMaximum: req.body.twoBMaximum,
          twoLMaximum: req.body.twoLMaximum,
          runBMaximum: req.body.runBMaximum,
          runLMaximum: req.body.runLMaximum,
          threeBFullPay: req.body.threeBFullPay,
          threeTFullPay: req.body.threeTFullPay,
          twoBFullPay: req.body.twoBFullPay,
          twoLFullPay: req.body.twoLFullPay,
          runBFullPay: req.body.runBFullPay,
          runLFullPay: req.body.runLFullPay,
          timeCancel: req.body.timeCancel
        });
      
        fullall.save(function(err){
          if(err){
            res.json({ success: false })
          } else {
            res.json({ success: true })
          }
        });        
      }
    }
  });

})

router.post('/update-fullmiddle',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'update-fullmiddle'
  })

  log.save(function(err){})

  var id = req.body.id
  var threeb = req.body.threeb
  var threet = req.body.threet
  var threel = req.body.threel
  var twob = req.body.twob
  var twol = req.body.twol
  var runb = req.body.runb
  var runl = req.body.runl

  ListLottery.findOne({
    id:id
  },function(err,lottery){
    console.log(lottery)
  })
});

dateAdd = function(date, interval, units) {
  var ret = new Date(date); //don't change original date
  var checkRollover = function() { if(ret.getDate() != date.getDate()) ret.setDate(0);};
  switch(interval.toLowerCase()) {
    case 'year':ret.setFullYear(ret.getFullYear() + units); checkRollover();  break;
    case 'quarter':ret.setMonth(ret.getMonth() + 3*units); checkRollover();  break;
    case 'month':ret.setMonth(ret.getMonth() + units); checkRollover();  break;
    case 'week':ret.setDate(ret.getDate() + 7*units);  break;
    case 'day':ret.setDate(ret.getDate() + units);  break;
    case 'hour':ret.setTime(ret.getTime() + units*3600000);  break;
    case 'minute':ret.setTime(ret.getTime() + units*60000);  break;
    case 'second':ret.setTime(ret.getTime() + units*1000);  break;
    default:ret = undefined;  break;
  }
  return ret.getTime();
}

router.post('/create-yeekee',function(req,res){
  i = 0; 
  e = 87;

  for(let index = i; index <= e; index++){
    Table.findOne({
      lottery:2
    }).sort({_id : -1}).exec(function(err,table){

      var dateOpen = new Date(table.date_open);
      var minStart = dateOpen.getMinutes() + 15 * index;

      var addTime = dateAdd(dateOpen,"minute",minStart);
          
      var yeekee = new YeeKee({
        round: index + 1,
        dateOpen : new Date(addTime).getTime(),
        tableid:table._id,
        open:1
      });

      yeekee.save(function(err){}); 
    });
  }
  
  res.json({success:true});
});

router.post('/list-yeekeelast',function(req,res){
  YeeKeeNumber.find({
    round:req.body.round,
    tableid:req.body.tableid
  }).sort({time_send:-1}).limit(16).exec(function(err,yeekeenumber){
    res.json(yeekeenumber)
  })
})

router.get('/list-yeekee/:tableid',function(req,res){
  arr = [];
  YeeKee.find({
    tableid:req.params.tableid
  }).sort({ round: 1}).exec(function(err,data){  
    if(err) return console.log(err);
    data.forEach(d => {
      var now = new Date();
      var dateOpen = new Date(d.dateOpen);
      var dateClose = new Date(d.dateClose);

      var day = dateOpen.getDate();
      var month = dateOpen.getMonth();
      var year = dateOpen.getFullYear();

      arr.push({ round: d.round , open:d.open , dateOpen: d.dateOpen , dateClose: d.dateClose })
    });

    res.json(arr)
  });

});

router.get('/list-yeekee',function(req,res){
  arr = [];
  Table.findOne({
    lottery:2
  }).sort({_id:-1}).exec(function(err,table){
    YeeKee.find({
      tableid:table._id
    }).sort({ round: 1}).exec(function(err,data){  
      if(err) return console.log(err);
      data.forEach(d => {
        var now = new Date();
        var dateOpen = new Date(d.dateOpen);
        var dateClose = new Date(d.dateClose);
  
        var day = dateOpen.getDate();
        var month = dateOpen.getMonth();
        var year = dateOpen.getFullYear();
  
        arr.push({ round: d.round ,open:d.open , dateOpen: d.dateOpen ,tableid: d.tableid , dateClose: d.dateClose })
      });
  
      res.json(arr)
    });
  })
});

router.post('/yeekee-onoff',function(req,res){
  YeeKee.updateOne({tableid:req.body.tableid, round:req.body.round},{ $set:{open:req.body.onoff}} ,function(err){
    if(err) { res.json(err) } else { res.json({success:true})}
  })
})

router.get('/list-tablefix',function(req,res){
  Tablefix.find({
  },function(err,tablefix){
    res.json(tablefix)
  })
})

router.get('/list-tablefix/:id',function(req,res){
  Tablefix.findOne({
    _id:req.params.id
  },function(err,tablefix){
    res.json(tablefix)
  })
})

router.post('/create-tablefix',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'create-tablefix'
  })

  log.save(function(err){})

  tablefix = new Tablefix({
    day:req.body.day,
    lottery:req.body.lottery,
    timeOpen:req.body.timeOpen,
    timeClose:req.body.timeClose,
    time_create:new Date().getTime(),
    open:1
  })

  tablefix.save(function(err){
    if(err){
      res.json({success:false})
    } else {
      res.json({success:true})
    }
  })
})

router.post('/edit-tablefix',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'edit-tablefix'
  })

  log.save(function(err){})

  Tablefix.updateOne({_id:req.body.id},{ $set:{timeOpen:req.body.timeOpen,timeClose:req.body.timeClose }},function(err){
    if(err) {
      res.json({success:false})
    } else {
      res.json({success:true})  
    }
  })
})

router.post('/open-tablefix',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'open-tablefix'
  })

  log.save(function(err){})

  Tablefix.updateOne({_id:req.body.id},{ $set:{open:req.body.status }},function(err){
    if(err) {
      res.json({success:false})
    } else {
      res.json({success:true})  
    }
  })
})

router.post('/delete-tablefix',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'delete-tablefix'
  })

  log.save(function(err){})

  Tablefix.deleteOne({_id:req.body.id},function(err){
    if(err){
      res.json({success:false})
    } else {
      res.json({success:true})
    }
  })
})

router.post('/list-yeekeedata',function(req,res){
  var arr = []
  var yeekee = []

  YeeKeeNumber.find({
    round:req.body.round,
    tableid:req.body.tableid
  }).sort({time_send:-1}).exec(function(err,yeekee){
    var total = 0
    yeekee.forEach(data => {
      total += parseInt(data.num)
      yeekee.push(data)
    })

    arr.push({'total':total,'yeekeeData':yeekee})

    res.json(arr)
  })
});

router.post('/create-yeekeelock',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'create-yeekeelock'
  })

  log.save(function(err){})

  var yeekeelock = new YeeKeeLock({
    threeb:req.body.threeb,
    twol:req.body.twol,
    round:req.body.round,
    tableid:req.body.tableid
  })

  yeekeelock.save(function(err){
    if(err){
      res.json({success:false})
    } else { 
      res.json({success:true})
    }
  })
})

router.post('/edit-yeekeelock',function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'edit-yeekeelock'
  })

  log.save(function(err){})

  YeeKeeLock.updateOne({ round:req.body.round , tableid:req.body.tableid } , { $set : { threeb:req.body.threeb , twol:req.body.twol }} , function(err,res){
    if(err) console.log(err)
  })  

  res.json({success:true})
})

router.post('/list-yeekeelock',function(req,res){
  YeeKeeLock.findOne({
    round:req.body.round,
    tableid:req.body.tableid
  },function(err,yeekee){
    res.json(yeekee)
  })
})

router.get('/list-lastyeekeetable/',function(req,res){
  YeeKee.findOne({
    round: req.params.id
  },function(err,data){
    if(err) return next(err)
    res.json(data)
  });

});

router.get('/list-yeekeelasttable/',function(req,res){
  Table.findOne({
    lottery:2
  }).sort({_id:-1}).exec(function(err,table){
    if(err) return next(err)
    res.json(table)
  })
})

router.post('/list-yeekeeformid/',function(req,res){
  YeeKee.findOne({
    round: req.body.id,
    tableid:req.body.tableid
  },function(err,data){
    if(err) console.log(err)
    res.json(data)
  })
})

router.post('/list-yeekeenumber',function(req,res){
  YeeKeeNumber.find({
    round: req.body.round,
    tableid:req.body.tableid
  }).sort({time_send:-1}).exec(function(err,data){
    if(err) console.log(err)
    res.json(data)
  })
})

router.post('/list-totalyeekeenumber',function(req,res){
  var total = 0
  YeeKeeNumber.find({
    round: req.body.round,
    tableid:req.body.tableid
  },function(err,data){
    if(err) console.log(err)
    data.forEach(d => {
      total += parseInt(d.num)
    })

    res.json(total)
  })
})

router.post('/yeekee-pay',function(req, res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'yeekee-pay'
  })

  log.save(function(err){})

  User.updateOne({ userid:req.body._id } , { $inc:{ credit:req.body.amount }} , function(err){
    if(err) console.log(err)
    creditlog = new CreditLog({
      detail:5,
      by:'',
      userid:user._id,
      username:user.username,
      credit:req.body.amount,
      balance:user.credit,
      time_create:new Date().getTime(),
      betid:b._id,
      note:'ได้รับเงินรางวัลจากการร่วมสนุกยิงเลขกับหวยยี่กี',
      mode:2
    })
  
    creditlog.save(function(err){
      if(err) console.log(err); else res.json({success: true});
    })

  })

})

router.post('/attack', function(req, res) {
  if(req.body.bot){
    var attack = new YeeKeeNumber({
      num:req.body.number,
      userid:'',
      username:req.body.username,
      time_send:new Date().getTime(),
      round:req.body.round,
      tableid:req.body.tableid,
    })

    attack.save(function(err){})
    res.json({success:true})

  } else {
    User.findOne({
      _id: req.body.id
    },function(err,user){
      if(err) console.log(err);
      var attack = new YeeKeeNumber({
        num:req.body.number,
        userid:user._id,
        username:user.username,
        time_send:new Date().getTime(),
        round:req.body.round,
        tableid:req.body.tableid,
      })

      attack.save(function(err){})
      res.json({success:true})
    });    
    
  }
})

router.get('/list-credit', function(req,res){

  var token = getToken(req.headers);
  if (token) {
    var userid = jwtDecode(token)
    User.findOne({
      _id: userid.id
    },function(err,user){
      CreditLog.find({
        userid:user._id
      }).sort({_id:-1}).exec(function(err,credit){
        res.json(credit)
      })
    });
    
  } else {
    res.json({ success: false });
  }

})


router.get('/logout', function(req,res){

  var token = getToken(req.headers);
  if (token) {
    var userid = jwtDecode(token)
    User.findOne({
      _id: userid.id
    },function(err,user){
      User.updateOne({ _id:user.id } , { $set : { token:"" }} , function(err,res){
        if(err) console.log(err)
      })
      res.json({success:true})
    });
  } else {
    res.json({ success: false });
  }

})

router.post('/list-affiliate' , function(req,res){
  var userD = Array()
  var totalBet = 0
  User.find({
    aff_userid:req.body.id
  },function(err,user){  
    user.forEach(u => {
      userD.push({'id':u._id,'username':u.username,'register_date':u.date_register,'totalbet':0})  
    })
    res.json(userD)
  })
})

router.get('/list-log', function(req,res){
  Log.find({

  }).sort({_id:-1}).exec(function(err,log){
    if(err) res.json(err); else res.json(log)
  })
})

router.post('/login-userrole', function(req,res){
  UserRole.findOne({
    username: req.body.username
  }, function(err, user) {
    try {
      if(!user){
        res.json({success: false , msg: 'โปรดตรวจสอบข้อมูลการล็อกอินให้ถูกต้อง!'})
        return false
      }

      if(!user.status){
          // check if password matches
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            // return the information including token as JSON

            var token = jwt.sign({'id':user._id , 'role':user.role , 'pages':user.pages}, config.secret);
            // User.updateOne({ username : req.body.username },{ $set: { token : token , last_login:new Date().getTime() }}, function(err,res){});
            res.json({success: true, token: 'JWT ' + token , msg: 'เข้าสู่ระบบเรียบร้อย'});
          } else {
            res.json({success: false, msg: 'รหัสผ่านผิดพลาด'});
          }
        });
      } else {
        res.json({success: false , msg: 'ไม่สามารถเข้าสู่ระบบได้กรุณาติดต่อผู้ดูแลระบบ' })
      }  
    } catch (error) {
      
    }
  })
});

router.post('/list-userrole', function(req,res){
  UserRole.findOne({
    _id:req.body.id
  },function(err,user){
    res.json(user)
  })
})

router.get('/list-userrole', function(req,res){
  UserRole.find({

  },function(err,user){
    res.json(user)
  })
})

router.post('/create-userrole', function(req,res){
  log = new Log({
    user:req.body.user,
    time_create:new Date().getTime(),
    mode:'create-userrole'
  })

  log.save(function(err){})

  var now = new Date().getTime()
  if(req.body.username == "" && req.body.password == "" && req.body.pages == ""){
    res.json({success:false, msg:'กรุณากรอกข้อมูลให้ครบถ้วน'})
  } else {
    user = new UserRole({
      username:req.body.username,
      password:req.body.password,
      role:'ADMIN',
      time_create:now,
      pages:req.body.pages
    })

    user.save(function(err){
      if(err) res.json({success:false})
      res.json({success:true})
    })    
  }
})

router.post('/edit-userrole', function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'edit-userrole'
  })

  log.save(function(err){})

  var now = new Date().getTime()
  if(req.body.pages == ""){
    res.json({success:false, msg:'กรุณากรอกข้อมูลให้ครบถ้วน'})
  } else {

    if(req.body.password){
      bcrypt.genSalt(10, function (err, salt) {
        if (err) console.log(err)
        bcrypt.hash(req.body.password, salt, null, function (err, hash) {
          if (err) console.log(err)
          UserRole.updateOne({ _id:req.body.id },{ $set : { password: hash }},function(err){})
        });
      });
    }

    UserRole.updateOne({ _id:req.body.id },{ $set : { pages:req.body.pages }},function(err,res){
      if(err) console.log(err)  
    }) 

    res.json({success:true})
  }
})

router.post('/delete-userrole', function(req,res){
  log = new Log({
    user:req.body.username,
    time_create:new Date().getTime(),
    mode:'delete-userrole'
  })

  log.save(function(err){})

  UserRole.deleteOne({_id:req.body.id},function(err){})
  
  res.json({success:true})
})
getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
